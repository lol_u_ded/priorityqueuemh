package exercise06.example2;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class MinHeap implements MyPriorityQueue {
    private Integer heap[];
    private int size;

    public MinHeap(int initCapacity) {
        this.heap = new Integer[initCapacity];
    }

    // For testing purpose only.
    public Integer[] getHeap() {
        return heap;
    }

    @Override
    public void insert(Integer elem) throws IllegalArgumentException {
        if(!isEmpty()) {
            int insertPos = size;
            if (insertPos >= heap.length) {
                int newSize = heap.length * 2;
                heap = Arrays.copyOf(heap, newSize);
            }
            heap[insertPos] = elem;
            upHeap(insertPos);
        }else{
            heap[0] = elem;
        }
        size++;
    }

    @Override
    public boolean isEmpty() {
        return size <= 0;
    }

    @Override
    public Integer min() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return heap[0];
        }
    }

    @Override
    public Integer removeMin() throws NoSuchElementException {
        Integer min = min();
        Integer lastElem = heap[getLastElement()-1];
        heap[0] = lastElem;
        heap[getLastElement()] = null;
        downHeap(0);
        size--;
        return min;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Integer[] toArray() {
        return Arrays.copyOf(heap, size);
    }

    private int getLastElement() throws NoSuchElementException {
        if (!isEmpty()) {
            return size()-1;
        } else {
            throw new NoSuchElementException();
        }
    }

    private boolean isRoot(int index) {
        return heap[index].equals(min());
    }

    private boolean isLowestLevel(int index) {
        int rightChildIdx = getRightChild(index);
        int leftChildIdx = getLeftChild(index);
        return rightChildIdx <= heap.length && leftChildIdx <= heap.length;
    }

    private void upHeap(int index) {
        if (!isRoot(index)) {
            int parent = getParent(index);
            if (heap[parent] > heap[index]) {
                swap(parent, index);
                upHeap(parent);
            }
        }
    }

    private void downHeap(int index) {
        if (!isLowestLevel(index)) {
            int rightChildIdx = getRightChild(index);
            int leftChildIdx = getLeftChild(index);
            if (heap[leftChildIdx] > heap[rightChildIdx]) {
                if (heap[rightChildIdx] < heap[index]) {
                    swap(rightChildIdx, index);
                    downHeap(rightChildIdx);
                }
            } else {
                if (heap[leftChildIdx] < heap[index]) {
                    swap(leftChildIdx, index);
                    downHeap(leftChildIdx);
                }
            }
        }

    }

    private int getParent(int index) {
        return (index - 1) / 2;
    }

    private int getLeftChild(int index) {
        return (2 * index) + 1;
    }

    private int getRightChild(int index) {
        return (2 * index) + 2;
    }

    private void swap(int index1, int index2) {
        Integer swapInt = heap[index1];
        heap[index1] = heap[index2];
        heap[index2] = swapInt;
    }

    public String toString() {
        Integer[] arr = toArray();
        StringBuilder stringBuilder = new StringBuilder();
        for(Integer elem : arr){
            stringBuilder
                    .append("[")
                    .append(elem)
                    .append("]");
        }
        return stringBuilder.toString();
    }

}
