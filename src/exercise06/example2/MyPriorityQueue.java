package exercise06.example2;

import java.util.NoSuchElementException;

public interface MyPriorityQueue {
	
	/**
	 * This method returns the PQ represented as an array.
	 * @return PQ as array.
	 */
	public Integer[] toArray();
		
	/**
	 * 
	 * @return True if the PQ is empty, false otherwise.
	 */
	public boolean isEmpty();	
	
	/**
	 * 
	 * @return the current size of the PQ.
	 */
	public int size();
	
	/**
	 * This method inserts an new element into the PQ.
	 * 
	 * @param elem to be inserted into the PQ.
	 * @throws IllegalArgumentException if elem is null.
	 */
	public void insert(Integer elem) throws IllegalArgumentException;
	
	/**
	 * This method removes and returns the minimum element from the PQ. 
	 * 
	 * @return the minimum element of the PQ, or null if the PQ is empty.
	 */
	public Integer removeMin() throws NoSuchElementException;
	
	/**
	 * This method returns the minimum element from the PQ without removing it.
	 * @return the minimum element of the PQ or null if the PQ is empty.
	 */
	public Integer min() throws NoSuchElementException;
}
