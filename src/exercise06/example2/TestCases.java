package exercise06.example2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCases {

    MinHeap testHeap;

    @BeforeEach
    public void beforeEach(){
        this.testHeap = new MinHeap(2);
    }

    @Test
    public void testInsertElem() {
        testHeap.insert(5);
        testHeap.insert(1);
        testHeap.insert(9);
        assertEquals(3,testHeap.size());
        assertEquals(4, testHeap.getHeap().length);
        assertEquals(1, testHeap.min().intValue());
        System.out.println(testHeap.toString());
    }

    @Test
    public void testRemoveElem() {
        testHeap.insert(5);
        testHeap.insert(1);
        testHeap.insert(9);
        testHeap.insert(6);
        testHeap.insert(10);
        testHeap.insert(8);
        assertEquals(6,testHeap.size());
        assertEquals(1,testHeap.removeMin().intValue());
        assertEquals(5, testHeap.size());
        assertEquals("[10][5][8][6][10]",testHeap.toString());
        System.out.println(testHeap.toString());
    }

    @Test
    public void testIsEmpty() {
        assertTrue(testHeap.isEmpty());
    }





}
